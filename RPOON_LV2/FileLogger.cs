﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV2
{
    class FileLogger:ILogger
    {
        private string filePath;

        public FileLogger(string FilePath)
        {
            this.filePath = FilePath;
        }
        
        public void Log(ILogable data)
        {
            using(System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.Write(data.GetStringRepresentation());
            }
        }
    }
}

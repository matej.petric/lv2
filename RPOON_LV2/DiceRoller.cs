﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV2
{
    class DiceRoller:ILogable
    {
        private List<Die> dice;
        private List<int> rollResult;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.rollResult = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            this.rollResult.Clear();
            foreach (Die die in dice)
            {
                this.rollResult.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.rollResult
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }

        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(int result in rollResult)
            {
                stringBuilder.Append(result + " ");
            }
            return stringBuilder.ToString();
        }
        
    }
}

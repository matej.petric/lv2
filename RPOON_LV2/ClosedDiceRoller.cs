﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV2
{
    class ClosedDiceRoller: IRollableDiceRoller
    {
        private List<Die> dice;
        private List<int> rollResult;
        public ClosedDiceRoller(int count, int numberOfSides)
        {
            this.dice = new List<Die>();
            for (int i = 0; i < count; i++)
            {
                this.dice.Add(new Die(numberOfSides));
            }
            this.rollResult = new List<int>();
        }
        public void RollAllDice()
        {
            this.rollResult.Clear();
            foreach (Die die in dice)
            {
                this.rollResult.Add(die.Roll());
            }
        }
    }
}

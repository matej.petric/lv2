﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV2
{
    interface IChangableDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
